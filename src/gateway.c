#include "gateway/modem.h"
#include "gateway/wireless.h"
#include "common/device.h"

/**
 * This function is polled by the main loop and should handle any packets coming
 * in over the modem or 868 MHz communication channel.
 */

/**
 * This function checks if the specified sensor is currently connected to
 * the gateway
 * support at: paardilab@unal.edu.co
 */
bool deviceIsActive(uint8_t const *data_modem[MODEM_MAX_PAYLOAD_LENGTH])
{
  device_id_t incoming_id;
  device_id_t active_id = get_device_id();
  for (int i = 1; i < 16; i++)
  {
    incoming_id.bytes[i - 1] = data_modem[0][i];
  }
  incoming_id.words[0] = (uint32_t)data_modem[0][17] | (uint32_t)data_modem[0][18] << 8 | (uint32_t)data_modem[0][19] << 16 | (uint32_t)data_modem[0][20] << 24;
  incoming_id.words[1] = (uint32_t)data_modem[0][21] | (uint32_t)data_modem[0][22] << 8 | (uint32_t)data_modem[0][23] << 16 | (uint32_t)data_modem[0][24] << 24;
  incoming_id.words[2] = (uint32_t)data_modem[0][25] | (uint32_t)data_modem[0][26] << 8 | (uint32_t)data_modem[0][27] << 16 | (uint32_t)data_modem[0][28] << 24;
  incoming_id.words[3] = (uint32_t)data_modem[0][29] | (uint32_t)data_modem[0][30] << 8 | (uint32_t)data_modem[0][31] << 16 | (uint32_t)data_modem[0][32] << 24;
  active_id = get_device_id();
  bool checker = true;
  for (int i = 0; i < 16; i++)
  {
    if (incoming_id.bytes[i] != active_id.bytes[i])
      checker = false;
  }
  for (int i = 0; i < 4; i++)
  {
    if (incoming_id.words[i] != active_id.words[i])
      checker = false;
  }

  return checker;
}

void handle_communication(void)
{
  /* we are always waiting for incoming info and constantly checking the input
  for this we need variables to receive data and sizes
  */
  uint8_t const *data_modem[MODEM_MAX_PAYLOAD_LENGTH]; // what comes from the modem
  size_t length[1];
  device_id_t incoming_device_id[1];
  uint8_t data_sensor[WIRELESS_PAYLOAD_LENGTH]; //what comes from the sensor
  bool sensor_sent, modem_sent; //flags 
  uint8_t modem_out[MODEM_MAX_PAYLOAD_LENGTH]; //what goes to the modem
  uint8_t wireless_out[WIRELESS_PAYLOAD_LENGTH]; //what goes to the sensor
  //device_id_t incoming_id;
  //device_id_t active_id;
  sensor_sent = wireless_dequeue_incoming(incoming_device_id, data_sensor); //reading sensor

  modem_sent = modem_dequeue_incoming(data_modem, length); //reading modem

  if (modem_sent)
  { //oh! we got some data here coming from the backend/modem
    /*Time to understand which data we just got*/

    if (data_modem[0][0] == 0xFF)
    { //PING

      modem_out[0] = 0xAA;
      modem_enqueue_outgoing(modem_out, 1); //Simple ponging from this gateway

      modem_out[0] = 0xAB;
      if (deviceIsActive(data_modem))
        modem_enqueue_outgoing(modem_out, 1); //Simple ponging from the device specified
    }
    else if (data_modem[0][0] == 0xFE)
    { //RESTART
      if (deviceIsActive(data_modem)) 
        reset_device(); //Restart specified sensor
    }
    else if (data_modem[0][0] == 0xFD)
    { //ADD TOKEN
      wireless_out[0] = 0x01;
      for (int i = 1; i <= 16; i++)
      {
        wireless_out[i] = data_modem[0][i];
      }
      wireless_enqueue_outgoing(get_device_id(), wireless_out); //resending token to add
    }
    else if (data_modem[0][0] == 0xFC)
    { //REMOVE TOKEN
      wireless_out[0] = 0x02;
      for (int i = 1; i <= 16; i++)
      {
        wireless_out[i]=data_modem[0][i];
      }
      wireless_enqueue_outgoing(get_device_id(), wireless_out); //resending token to remove 
    }
    else if (data_modem[0][0] == 0xFB)
    { //OPEN DOOR
      wireless_out[0] = 0x00;
      wireless_enqueue_outgoing(get_device_id(), wireless_out); //Order to open door
    }
  }

  if (sensor_sent)
  { //We just got something coming from the sensor
      modem_out[0] = data_sensor[0];
      modem_enqueue_outgoing(modem_out, 1); //resending sensor info to the outer world
  }
}
