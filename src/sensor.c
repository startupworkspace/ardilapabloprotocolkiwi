#include "sensor/wireless.h"
#include "sensor/ki_store.h"
#include "sensor/door.h"
#include "common/device.h"

/**
 * This function is polled by the main loop and should handle any packets coming
 * in over the 868 MHz communication channel.
 * support at: paardilab@unal.edu.co
 */

uint8_t decode(ki_store_result_t result){
  switch (result)
  {
    case KI_STORE_SUCCESS:
      return 0xFA;
    case KI_STORE_ERROR_FULL:
      return 0xF9;
    case KI_STORE_ERROR_UNKNOWN:
      return 0xF8;
    default:
      return 0xF8;
  }
}

void handle_communication(void)
{
  //This function decides what to do when we get packets coming to the sensor and
  //generate packets coming from the sensor
  uint8_t sensor_data[WIRELESS_PAYLOAD_LENGTH]; //what arrives to the sensor
  bool gateway_sent; //flag
  ki_store_result_t token_result;
  uint8_t code; //what leaves the sensor
  gateway_sent = wireless_dequeue_incoming(sensor_data); //reading sensor input

  if (gateway_sent)
  { //true: packet just arrived to the sensor

    if (sensor_data[0] == 0x00)
    {//TRIGGER
      door_trigger();
    }
    else if (sensor_data[0] == 0x01)
    {//ADD TOKEN
      token_result = ki_store_add(sensor_data);
      code=decode(token_result);
      wireless_enqueue_outgoing(&code); //Writing result back to the gateway
    }
    else if (sensor_data[0] == 0x02)
    {//REMOVE TOKEN
      token_result = ki_store_remove(sensor_data);
      code=decode(token_result);
      wireless_enqueue_outgoing(&code); //Writing result back to the gateway
    }
  }
}
